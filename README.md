# pymunk-control

Describe, simulate and control simple two-dimensional robots and environments.
Powered by [pymunk](https://www.pymunk.org/en/latest/).

![Worm locomotion](examples/locomotion/main.webm)
