"""A simple genetic algorithm for optimizing simple trajectories.

In pymunk_robots an action is an list with angular velocities for each joint.
We call 'trajectory' to a list of actions. Our approach is to use a simple
genetic algorithm to approximately solve:

    trajectory = max[trajectory] Reward(trajectory)

For a fixed set of initial conditions, we optimize linear policies
(which induce trajectories).
"""
import random
import concurrent.futures
from dataclasses import dataclass
import math
import statistics
import json
import time

from pymunk_robots.utils import get_action_space_size
from pymunk_robots.parts import Link
from pymunk_robots.scenes import Scene
from pymunk_robots.utils import build_pymunk_space
from pymunk_robots.utils import get_robot_position
from pymunk_robots.utils import get_joint_positions
from pymunk_robots.utils import get_joint_velocities
from pymunk_robots.utils import get_joint_forces
from pymunk_robots.utils import apply_action
from pymunk_robots.utils import get_robot_bounding_box
import pymunk


@dataclass(frozen=True, eq=True)
class LinearPolicy:
    """A simple linear function."""
    weights: tuple[tuple[float, ...], ...]

    def __call__(
            self,
            observation: tuple[float, ...]
            ) -> tuple[float, ...]:
        return tuple(
            sum(o*w for o, w in zip(observation, weights))
            for weights in self.weights
        )


def get_observation(space: pymunk.Space) -> list[float]:
    """Return a list with the normalized angle positions and
    velocities."""
    positions = get_joint_positions(space)
    velocities = get_joint_velocities(space)
    observation = [a/(2*math.pi) for a in positions + velocities]
    return observation


def reward(
        robot: Link,
        scene: Scene,
        genotype: list[float],
        timesteps: int,
        ) -> float:
    """Simulate the robot in the scene using the phenotype corresponding
    to the given genotype and return the signed distance traveled."""
    # Build a pymunk space
    space = build_pymunk_space(robot, scene)

    # Convert the genotype into a control policy
    n = int(len(genotype)**(1/2))
    weights = tuple(
        tuple(w for w in genotype[i:i+n])
        for i in range(0, len(genotype), n)
    )
    policy = LinearPolicy(weights)

    # Simulate the system
    dt = scene.dt
    costs = list()
    for _ in range(timesteps):
        # Apply the action and step the simulation
        observation = get_observation(space)
        raw_action = policy(tuple(observation))
        action = [a*2*math.pi for a in raw_action]
        apply_action(action, space)
        space.step(dt)
        forces = get_joint_forces(space, dt)
        energy = statistics.mean([abs(f) for f in forces])
        costs.append(energy)

    # Return the signed distance traveled
    x, _ = get_robot_position(space)
    return x - statistics.mean(costs)


def random_genotype(n: int) -> list[float]:
    """Return a random genotype.
    """
    return [random.uniform(-1, 1) for _ in range(n)]


def mutated(g1: list[float]) -> list[float]:
    """Perturb g with a normal distribution."""
    return [a+random.gauss(0.0, 0.01) for a in g1]


def cross_over(
        g1: list[float], g2: list[float]
        ) -> list[float]:
    """Cross-over g1 and g2 by randomly combining their genes.
    """
    g = list()
    t = random.random()
    for g1i, g2i in zip(g1, g2):
        g.append(t*g1i + (1-t)*g2i)
    return g


def genetic(
        robot: Link,
        scene: Scene,
        timesteps: int,
        iterations: int,
        fresh_n: int,
        cross_over_n: int,
        elite_n: int,
        mutation_n: int,
        worker_n: int,
        ) -> list[float]:
    executor = concurrent.futures.ProcessPoolExecutor(worker_n)

    # Create a population
    start_time = time.time()
    genotype_len = len(get_observation(build_pymunk_space(robot, scene)))**2
    population = [
        random_genotype(genotype_len)
        for _ in range(fresh_n+cross_over_n+elite_n+mutation_n)
    ]

    # Define a short-hand for the reward function
    def get_rewards(population: list[list[float]]) -> list[float]:
        futures = [
            executor.submit(
                reward,
                robot,
                scene,
                genotype,
                timesteps
            )
            for genotype in population
        ]
        return [future.result() for future in futures]

    # Iterate the optimization loop
    for i in range(iterations):
        # Evaluate the fitness of each individual
        rewards = get_rewards(population)

        # Use the rewards as weights
        weights = list(map(lambda r: r-min(rewards), rewards))
        if sum(weights) == 0:
            weights = [1 for _ in weights]

        # Identify elite
        sorted_is = sorted(range(len(rewards)), key=lambda i: -rewards[i])
        elite = population[sorted_is[0]]

        # Build new population
        new_population = list()

        # Add elites
        elites = [population[i] for i in sorted_is[:elite_n]]
        new_population.extend(elites)

        # Sample fresh individuals
        for _ in range(fresh_n):
            g = random_genotype(genotype_len)
            new_population.append(g)

        # Mutate individuals, sampling them proportional to reward
        for _ in range(mutation_n):
            g1 = random.choices(population, weights, k=1)[0]
            g = mutated(g1)
            new_population.append(g)

        # Cross individuals, sampling them proportional to reward
        for _ in range(cross_over_n):
            g1, g2 = random.choices(population, weights, k=2)
            g = cross_over(g1, g2)
            new_population.append(g)

        # Update the population
        population = new_population

        # Print the state of the algorithm
        status = dict(
            reward_min=min(rewards),
            reward_max=max(rewards),
            reward_mean=statistics.mean(rewards),
            reward_stdev=statistics.stdev(rewards),
            iteration_i=i,
            iteration_target=iterations,
            seconds_since_start=round(time.time()-start_time, 1)
        )
        print(json.dumps(status, indent=4))

    return elite


def genotype_to_actions(
        robot: Link,
        scene: Scene,
        genotype: list[float],
        timesteps: int,
        ) -> list[list[float]]:
    """Simulate the robot in the scene using the phenotype corresponding
    to the given genotype and return the computed actions."""
    # Build a pymunk space
    space = build_pymunk_space(robot, scene)

    # Convert the genotype into a policy
    n = int(len(genotype)**(1/2))
    weights = tuple(
        tuple(w for w in genotype[i:i+n])
        for i in range(0, len(genotype), n)
    )
    policy = LinearPolicy(weights)

    # Simulate the system
    dt = scene.dt
    actions = list()
    for _ in range(timesteps):
        # Apply the action and step the simulation
        observation = get_observation(space)
        raw_action = policy(tuple(observation))
        action = [a*2*math.pi for a in raw_action]
        apply_action(action, space)
        space.step(dt)
        actions.append(action)

    # Return the actions
    return actions


def optimize_locomotion(
        robot: Link,
        scene: Scene,
        timesteps: int,
        iterations: int,
        fresh_n: int,
        cross_over_n: int,
        elite_n: int,
        mutation_n: int,
        worker_n: int,
        ) -> list[list[float]]:
    """Return a sequence of control inputs for the given robot."""
    # Run the genetic algorithm
    genotype = genetic(
        robot,
        scene,
        timesteps=timesteps,
        iterations=iterations,
        fresh_n=fresh_n,
        cross_over_n=cross_over_n,
        elite_n=elite_n,
        mutation_n=mutation_n,
        worker_n=worker_n,
    )

    # Return the best action
    return genotype_to_actions(robot, scene, genotype, timesteps)
