"""Trajectory optimization for a simple bipedal robot."""
from pathlib import Path

from pymunk_robots.parts import Link
from pymunk_robots.scenes import Scene
from pymunk_robots.scenes.obstacles import StaticSegment
from pymunk_robots.utils import plot_trajectory

from optimization import optimize_locomotion


MP4_PATH = Path(Path(__file__).name).with_suffix(".webm")


def build_robot() -> Link:
    """A simple chain robot."""
    # All the links in the robot have the same properties defined in this
    # little helper function.
    def ChainLink():
        """A simple chain link."""
        return Link(
            length=1.0,
            radius=0.1,
            mass=1.0,
            children=list(),
            children_gap=0.1,
            joint_max_force=10,
            friction_coefficient=1.5,
            elasticity=0.1,
        )

    # Build a chain robot
    n_links = 4
    root = ChainLink()
    link = root
    for i in range(n_links-1):
        new_link = ChainLink()
        link = link.children.append(new_link)
        link = new_link

    # Return the root of the graph
    return root


def build_scene() -> Scene:
    """A simple scene with a floor."""
    obstacles = list()

    # Add a floor
    obstacles.append(
        StaticSegment(
            p1=(-10.0, 0),
            p2=(10000.0, 0),
            radius=0.1,
            friction_coefficient=0.5,
            elasticity=0.1,
        )
    )

    # Build the scene with all the obstacles
    scene = Scene(
        gravity=9.81,
        obstacles=obstacles,
        fps=60,
        dt=1.0/60,
    )

    return scene


def main():
    """Build a simple robot, optimize a trajectory for locomotion and
    plot the result as an animation."""
    robot = build_robot()
    scene = build_scene()
    trajectory = optimize_locomotion(
        robot,
        scene,
        timesteps=600,
        iterations=100,
        fresh_n=10,
        cross_over_n=50,
        mutation_n=100,
        elite_n=40,
        worker_n=8,
    )
    plot_trajectory(robot, scene, trajectory, MP4_PATH)
    print(f"Wrote {MP4_PATH}")


if __name__ == "__main__":
    main()
