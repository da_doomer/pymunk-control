"""A static segment that collides with robots."""
from dataclasses import dataclass
import pymunk

@dataclass(eq=True, frozen=True)
class StaticSegment:
    """A static segment has start and end positions and a radius."""
    p1: tuple[float, float]
    p2: tuple[float, float]
    radius: float
    friction_coefficient: float
    elasticity: float

    def to_pymunk_objects(self) -> tuple[pymunk.Body, pymunk.Segment]:
        """Return new pymunk objects that represent `self`."""
        body = pymunk.Body(body_type=pymunk.Body.STATIC)
        segment = pymunk.Segment(body, self.p1, self.p2, self.radius)
        segment.elasticity = self.elasticity
        segment.friction = self.friction_coefficient
        return body, segment
