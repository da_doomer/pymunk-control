"""Classes representing scenes meant to contain robots."""
from .scene import Scene
