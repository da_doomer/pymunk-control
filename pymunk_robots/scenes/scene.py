"""A scene is a collection of obstacles."""
from dataclasses import dataclass
from pymunk_robots.scenes.obstacles import StaticSegment
import pymunk

@dataclass(eq=True, frozen=True)
class Scene:
    """A scene is a collection of obstacles and a gravity value."""
    obstacles: tuple[StaticSegment]
    gravity: float
    fps: int
    dt: float

    def to_pymunk_objects(self) -> list[tuple[pymunk.Body, pymunk.Segment]]:
        """Return new pymunk objects that represent `self`."""
        return [obstacle.to_pymunk_objects() for obstacle in self.obstacles]
