"""A collection of functions to interact between pymunk_robots and pymunk."""
from pathlib import Path
from functools import reduce
import shutil
import math

from pymunk_robots.parts import Link
from pymunk_robots.scenes import Scene
from pymunk_robots.scenes.obstacles import StaticSegment
import pymunk

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import pymunk.matplotlib_util

import ffmpeg

# TODO: split into submodules

# TODO: all these implementations assume there is a single robot as constructed by the Link class, and that all polys belong to the robot. It may be good to weaken these assumptions.


def build_pymunk_space(robot: Link, scene: Scene) -> pymunk.Space:
    """Return a `pymunk.Space` with all the parts of the robot."""
    space = pymunk.Space()
    space.gravity = (0, -scene.gravity)

    # Add all objects from the scene to the space
    for body, segment in scene.to_pymunk_objects():
        space.add(body, segment)

    # Add all objects from the robot to the space
    bodies, polys, constraints = robot.to_pymunk_objects(
        position=(0, 1)
    )
    for thing in bodies + polys + constraints:
        space.add(thing)

    return space


def get_action_space_size(robot: Link) -> int:
    """Return the number of dimensions of the action space. I.e. the number
    of actuators."""
    _, _, constraints = robot.to_pymunk_objects(position=(0, 0))
    motor_n = len(list(filter(
        lambda constraint: isinstance(
            constraint, pymunk.constraints.SimpleMotor
        ),
        constraints,
    )))
    return motor_n


def get_robot_bounding_box(space: pymunk.Space) -> pymunk.bb.BB:
    """Returns the bounding box of all polygons in the space."""
    shapes = filter(
        lambda shape: isinstance(shape, pymunk.shapes.Poly),
        space.shapes
    )
    bounding_boxes = [shape.cache_bb() for shape in shapes]
    bounding_box = reduce(
        lambda b1, b2: b1.merge(b2),
        bounding_boxes
    )
    return bounding_box


def get_robot_position(space: pymunk.Space) -> tuple[float, float]:
    """Returns the center of the robot's bounding box."""
    c = get_robot_bounding_box(space).center()
    return (c.x, c.y)


def get_joint_velocities(space: pymunk.Space) -> list[float]:
    """Returns the joint velocities (i.e. the relative angular velocities of
    each pair of links related by a joint)."""
    pivots = filter(
        lambda constraint: isinstance(
            constraint, pymunk.constraints.PivotJoint
        ),
        space.constraints,
    )
    velocities = list()
    for pivot in pivots:
        velocity = (pivot.a.angular_velocity - pivot.b.angular_velocity)
        velocities.append(velocity)
    return velocities


def get_joint_positions(space: pymunk.Space) -> list[float]:
    """Returns the joint positions (i.e. the angle between each pair
    of connected links, relative to the joint that connects them)."""
    pivots = filter(
        lambda constraint: isinstance(
            constraint, pymunk.constraints.PivotJoint
        ),
        space.constraints,
    )
    positions = list()
    for pivot in pivots:
        pivot_position = pivot.a.local_to_world(pivot.anchor_a)
        a = pivot.a.position - pivot_position
        b = pivot.b.position - pivot_position
        degrees = a.get_angle_between(b)
        positions.append(degrees)
    return positions


def get_joint_forces(space: pymunk.Space, dt: float) -> list[float]:
    """Returns the joint most recently applied forces. Forces are
    computed dividing the most recent impulse by the (given) dt."""
    motors = filter(
        lambda constraint: isinstance(
            constraint, pymunk.constraints.SimpleMotor
        ),
        space.constraints,
    )
    forces = list()
    for motor in motors:
        force = motor.impulse/dt
        forces.append(force)
    return forces


def apply_action(action: list[float], space: pymunk.Space):
    """Applies the given action to the motors in the given pymunk.Space. Each
    entry in the given list is interpreted as a desired angular velocity
    of a motor (normalized by 2*pi)."""
    # Get the motors in the space
    motors = filter(
        lambda constraint: isinstance(
            constraint, pymunk.constraints.SimpleMotor
        ),
        space.constraints,
    )

    for a, motor in zip(action, motors):
        motor.rate = a


def plot_trajectory(
        robot: Link,
        scene: Scene,
        actions: list[list[float]],
        mp4_path: Path
        ):
    """Plot the given trajectory to the given path as a video."""
    # Build a pymunk space with the robot and the scene
    space = build_pymunk_space(robot, scene)

    # Plot each timestep
    output_dir = mp4_path.with_suffix(".movie_temp_dir")
    output_dir.mkdir()
    bb = get_robot_bounding_box(space)
    camera_size = max(
        bb.top-bb.bottom,
        bb.right-bb.left,
    )
    fps = scene.fps
    dt = scene.dt
    for t in range(len(actions)):
        fig = Figure()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot()
        options = pymunk.matplotlib_util.DrawOptions(ax)
        options.flags = pymunk.SpaceDebugDrawOptions.DRAW_SHAPES
        space.debug_draw(options)
        cx, cy = get_robot_position(space)
        ax.set_xlim(cx-camera_size, cx+camera_size)
        ax.set_ylim(cy-camera_size, cy+camera_size)
        frame_path = output_dir/(f"{t}.png").rjust(10, "0")
        fig.savefig(frame_path)

        # Apply the action and step the simulation
        apply_action(actions[t], space)
        space.step(dt)

    (
        ffmpeg
        .input(output_dir/"*.png", pattern_type="glob", framerate=fps)
        .output(str(mp4_path))
        .overwrite_output()
        .run(quiet=True)
    )

    shutil.rmtree(output_dir)
